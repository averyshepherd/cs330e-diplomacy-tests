#!/usr/bin/env python3


# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve



class TestDiplomacy (TestCase):
    # ----
    # solve
    # ----
    # no input case 1
    def test_solve_1(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),"")
    # no input case 2
    def test_solve_2(self):
        r = StringIO("\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),"")
    # stand alone
    def test_solve_3(self):
        r = StringIO("A Austin Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),"A Austin\n")
    # move to city never EXISTS 
    def test_solve_4(self):
        r = StringIO("A Austin Move Boston\nC California Move Boston\n")
        #r = StringIO("A Austin Move Boston\nC California Move Dallas\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nC [dead]\n")
        # self.assertEqual(
        #     w.getvalue(),"A Boston\nC Dallas\n")
    # cycle case
    def test_solve_5(self):
        r = StringIO("A Austin Move Boston\nB Boston Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Boston\nB Austin\n")
    # 1 v 1
    def test_solve_6(self):
        r = StringIO("A Austin Hold\nB Boston Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\n")
    # unequal support
    def test_solve_7(self):
        r = StringIO("A Austin Hold\nB Boston Move Austin\nC California Support A\nD Dallas Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),"A Austin\nB [dead]\nC California\nD [dead]\n")
    # equal support 2 2 1 supporters don't die
    def test_solve_8(self):
        r = StringIO("A Austin Move Boston\nB Boston Hold\nC California Support A\nD Dallas Support B\nE Eugene Move Boston\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC California\nD Dallas\nE [dead]\n")
    # voided support
    def test_solve_9(self):
        r = StringIO("A Austin Move Boston\nB Boston Hold\nC California Support A\nD Dallas Move California\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD [dead]\n") 
    # complex1 and switch input order
    def test_solve_10(self):
        r = StringIO("A Austin Move Boston\nD Dallas Move California\nC California Support A\nB Boston Hold\nE Eugene Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC California\nD [dead]\nE Eugene\n")        
    # complex2
    def test_solve_11(self):
        r = StringIO("A Austin Hold\nB Boston Move Austin\nC California Move Boston\nD Dallas Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB Austin\nC Boston\nD Dallas\n") 

# ---
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()

